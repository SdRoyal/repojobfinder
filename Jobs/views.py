from django.shortcuts import render
from django.http import HttpResponse



def home(request):
	context = {"page_title" : "Home"}
	return render(request,"Jobs/home.html",context)


def about(request):
	context = {"page_title" : "About Us"}
	return render(request,"Jobs/about.html",context)


def contact(request):
	context = {"page_title" : "Contact Us"}
	return render(request,"Jobs/contact.html",context)


def findJob(request):
	context = {"page_title" : "Find a Jobs"}
	return render(request,"Jobs/find-job.html",context)


def jobDetail(request):
	context = {"page_title" : "Job Detail"}
	return render(request,"Jobs/job-details.html",context)


def blogs(request):
	context = {"page_title" : "Blogs"}
	return render(request,"Jobs/blogs.html",context)


def blogDetail(request):
	context = {"page_title" : "Blog Detail"}
	return render(request,"Jobs/blog-detail.html",context)