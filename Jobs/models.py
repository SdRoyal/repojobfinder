from django.db import models
from django.utils.timezone import now
# Use For Not Equle Build Query
from django.db.models import Q
# Use For Validation
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as __


brachTypeChoiceList = [
						("Main","Main"),
						("Sub","Sub")
					]

class Branch(models.Model):

	branch_type	 	= models.CharField(max_length=50,choices=brachTypeChoiceList,default="Sub",blank=False)
	email_id 		= models.EmailField(blank=False)
	mobile_no 		= models.CharField(max_length=13,blank=False)
	fax 			= models.CharField(max_length=15,blank=True)
	city            = models.CharField(max_length=20,blank=False)
	zipcode         = models.CharField(max_length=8,blank=False)
	address 		= models.TextField(blank=False)
	google_location = models.TextField(blank=False)
	opening_date	= models.DateField(blank=False,default=now)
	follow_on 		= models.TextField(blank=False)
	create_at		= models.DateTimeField(auto_now_add=True,editable=True)
	modify_at		= models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.email_id

	class Meta:
		verbose_name_plural = "Branches"
		db_table 			= "tbl_branch"


	def clean(self):
		if self.branch_type=="Main":
			if self.id is None:
				cnt = Branch.objects.filter(branch_type=self.branch_type).count()
			else :
				cnt = Branch.objects.filter(~Q(id=self.id),branch_type=self.branch_type).count()				
			if cnt >= 1:
				raise ValidationError({'branch_type':['Branch type Main already exist',]})	
		return self