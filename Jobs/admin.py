from django.contrib import admin
from django import forms
from tinymce.widgets import TinyMCE
from .models import *

class BranchForm(forms.ModelForm):
	class Meta:
		model   = Branch
		fields  = '__all__'
		widgets = {
			'address'           : forms.Textarea(attrs={'cols': 40, 'rows': 5,'style':'resize:none'}),
			'google_location'   : forms.Textarea(attrs={'cols': 40, 'rows': 5,'style':'resize:none'}),
			'follow_on'         : TinyMCE()
		}
	

@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
	form 				= BranchForm
	list_filter 		= ('branch_type','opening_date',)
	list_display    	= ('branch_type','city','email_id','mobile_no','fax','address',)
	search_fields		= ('email_id','address','city','mobile_no')
	# empty_value_display = '-empty-'
	# date_hierarchy 	= 'opening_date'
	# fields            = ('branch_type',('mobile_no','email_id'),'address','google_location','create_at','modify_at',)
	fieldsets       	= (
							(None,{
								'fields':(('email_id','branch_type'),('mobile_no','fax'),('opening_date'),)
							}),
							("Address Details",{
								'fields':(('city','zipcode'),('address','google_location'),)
							}),
							("Other Details",{
								'classes': ('collapse',),
								'fields':('follow_on',)
							}),
						)
