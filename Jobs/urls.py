from django.urls import path
from . import views


urlpatterns = [
    path('', views.home,name="Home"),
    path('about', views.about,name="About"),
    path('contact', views.contact,name="Contact"),
    path('findjob', views.findJob,name="FindJob"),
    path('jobdetail', views.jobDetail,name="JobDetail"),
    path('blogs', views.blogs,name="Blogs"),
    path('blog-detail', views.blogDetail,name="BlogDetail"),
]

