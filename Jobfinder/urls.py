from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.models import User, Group

# admin.site.unregister(User)
# admin.site.unregister(Group)

admin.site.site_header 	= "Job Finder Admin"
admin.site.site_title 	= "Job Finder Admin"
admin.site.index_title 	= "Dashboard"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Jobs.urls')),
]
